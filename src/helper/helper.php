<?php



function hashedAsset($x)
{
    $version = \Illuminate\Support\Facades\Cache::remember('gitVersion',5000,function(){
        $version = \GitVersion\Version::make(
            base_path()
        );
        return $version->getHash();
    });
    return asset($x).'?version='.$version;
}
