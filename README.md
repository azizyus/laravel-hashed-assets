# DOC


## INSTALLATION
``
composer require azizyus/laravel-hashed-assets
``

## USAGE
``
hashedAsset('https://example.com/test.js') gives 'https://example.com/test.js?version=_YOUR_GIT_HASH_'
``
